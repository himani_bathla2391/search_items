//
//  ViewController.m
//  search items
//
//  Created by Clicklabs 104 on 10/5/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"
NSMutableArray *list;
NSArray *pic;
NSMutableArray *result;
@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UISearchBar *search;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation ViewController

@synthesize table;
@synthesize search;
@synthesize button;
- (void)viewDidLoad {
    [super viewDidLoad];
    list=[NSMutableArray arrayWithObjects:@"Big Spicy Paneer Wrap",@"McSpicy Chicken",@"McSpicy Paneer",@"Maharaja Mac",@"Filet-O-Fish",@"Salad wrap",@"McChicken",@"McVeggie",@"Chicken McGrill",@"McEgg Burger",@"McAloo Tikki",@"French Fries",@"Hamburger",@"CheeseBurger",@"McFlurry",nil];
    result = [NSMutableArray arrayWithArray:list];
    [table reloadData];
    result = [NSMutableArray arrayWithCapacity:[list count]];
    table.hidden=YES;
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)button:(id)sender {
    UIAlertView *messageAlert = [[UIAlertView alloc]initWithTitle:@"Order is done" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [messageAlert show];
    table.hidden=YES;

}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
    result = [NSMutableArray arrayWithArray: [list filteredArrayUsingPredicate:resultPredicate]];
    [table reloadData];
    table.hidden=NO;
   
  }

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
 return 1;
 }

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
 return [result count];
 }

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView
                              dequeueReusableCellWithIdentifier:@"cellReuse"];
  
           cell.textLabel.text = result[indexPath.row];
    
    
   // cell.textLabel.text = list[indexPath.row];
   // cell.imageView.image = [UIImage imageNamed:[pic objectAtIndex:indexPath.row]];
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertView *messageAlert = [[UIAlertView alloc]initWithTitle:@"Item Selected" message:@"You've selected an item" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [messageAlert show];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    search.text = nil;
    
}



@end
