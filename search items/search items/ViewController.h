//
//  ViewController.h
//  search items
//
//  Created by Clicklabs 104 on 10/5/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDataSource,UITabBarDelegate,UISearchDisplayDelegate>


@end

